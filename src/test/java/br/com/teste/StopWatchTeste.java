package br.com.teste;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import br.com.servicos.StopWatch;


public class StopWatchTeste 
{	

	@Test
	public void stopWatch_deve_comecar_a_contar()
	{
		StopWatch sw = new StopWatch();
		sw.start();
		assertNotNull(sw);
	}
	
}
