package br.com.teste;


import static org.junit.Assert.assertNotNull;

import java.util.UUID;

import org.junit.Test;

import br.com.modelos.NovaCobranca;
import br.com.modelos.NovoCartaoDeCredito;


public class NovoCartaoDeCreditoTeste 
{	
	@Test
	public void email_Request_init()
	{
		NovoCartaoDeCredito nc = new NovoCartaoDeCredito();
		assertNotNull(nc);
	}
	
	@Test
	public void cobranca_request_constructor()
	{
		NovoCartaoDeCredito er = new NovoCartaoDeCredito("nome", "123", "555", "56");
		assertNotNull(er);
	}
	
	
}
