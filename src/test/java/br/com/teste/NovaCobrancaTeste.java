package br.com.teste;


import static org.junit.Assert.assertNotNull;

import java.util.UUID;

import org.junit.Test;

import br.com.modelos.NovaCobranca;


public class NovaCobrancaTeste 
{	
	@Test
	public void email_Request_init()
	{
		NovaCobranca nc = new NovaCobranca();
		assertNotNull(nc);
	}
	
	@Test
	public void cobranca_request_constructor()
	{
		NovaCobranca er = new NovaCobranca(2.2, UUID.randomUUID().toString());
		assertNotNull(er);
	}
	
	
}
