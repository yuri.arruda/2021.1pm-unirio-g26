package br.com.teste;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.com.ErrorResponse;
import br.com.modelos.Email;
import br.com.servicos.EmailService;

public class EmailServiceTeste 
{
	EmailService email;
	
	@Before
	public void starter() 
	{
		email.start();
	}
	
	@After
	public void finisher()
	{
		email.stop();
	}
	
	@Test
	public void email_inicializou()
	{
		email = null;
		email = email.start();
		assertNotNull(email);
	}
	
	@Test
	public void email_falhou_inicializar()
	{
		email = null;
		assertNull(email);
		email = email.start();
	}
	
	@Test
	public void deve_retornar_todos_emails()
	{
		email = email.start();
		
		ArrayList<Email> e = email.getAllEmails();

		assertTrue(e.size() > 0);
	}
	
	@Test
	public void deve_retornar_true_caso_email_seja_valido()
	{
		email = email.start();
		
		String emailValido01 = "teste@teste.com";
		String emailValido02 = "teste@teste.br";
		String emailValido03 = "teste@teste.co.uk";
		
		assertTrue(email.isEmailValid(emailValido01));
		assertTrue(email.isEmailValid(emailValido02));
		assertTrue(email.isEmailValid(emailValido03));

	}
	
	@Test
	public void deve_retornar_false_caso_email_seja_invalido()
	{
		email = email.start();
		
		String emailInvalido01 = "teste@teste";
		String emailInvalido02 = "testeAteste.com";
		String emailInvalido03 = "teste@teste.com.";
		assertTrue(email.isEmailValid(emailInvalido01));
		assertFalse(email.isEmailValid(emailInvalido02));
		assertFalse(email.isEmailValid(emailInvalido03));
	}
	
	@Test
	public void deve_retornar_false_caso_email_verificado_seja_nulo_ou_vazio()
	{
		email = email.start();
		
		String emailNulo = null;
		String emailVazio = "";
		assertFalse(email.isEmailValid(emailNulo));
		assertFalse(email.isEmailValid(emailVazio));
	}
	
	@Test
	public void deve_retornar_true_caso_email_exista()
	{
		email = email.start();
		
		String emailExistente = "email02@uniriotec.br";
		
		assertTrue(email.isEmailExist(emailExistente));
	}
	
	@Test
	public void deve_retornar_false_caso_email_nao_exista()
	{
		email = email.start();
		
		String emailInexistente = "email0000000@uniriotec.br";
		assertFalse(email.isEmailExist(emailInexistente));
	}
	
	@Test
	public void deve_retornar_false_caso_email_existente_verificado_seja_nulo_ou_vazio()
	{
		email = email.start();
		
		String emailNulo = null;
		String emailVazio = "";
		assertFalse(email.isEmailExist(emailNulo));
		assertFalse(email.isEmailExist(emailVazio));
	}
	
	@Test
	public void deve_enviar_email()
	{
		email = email.start();
		
		String email01 = "email02@uniriotec.br";
		String msg01 = "teste 123";
		Object e = email.sendEmail(email01, msg01);
		Email x = (Email) e;
		assertEquals(email01, x.getEmail());

	}
	
	@Test
	public void nao_deve_enviar_email_caso_email_seja_nulo()
	{
		email = email.start();
		
		String email01 = null;
		String msg01 = "teste 123";
		Object e = email.sendEmail(email01, msg01);
		ErrorResponse x = (ErrorResponse) e;
		assertEquals("1G", x.codigo);
	}
	
}
