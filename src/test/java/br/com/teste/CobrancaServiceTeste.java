package br.com.teste;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.com.ErrorResponse;
import br.com.modelos.Cobranca;
import br.com.modelos.Email;
import br.com.servicos.CobrancaService;
import br.com.servicos.EmailService;

public class CobrancaServiceTeste 
{
	CobrancaService cobranca;
	
	@Before
	public void starter() 
	{
		cobranca.start();
	}
	
	@After
	public void finisher()
	{
		cobranca.stop();
	}
	
	@Test
	public void cobranca_inicializou()
	{
		cobranca = null;
		cobranca =cobranca.start();
		assertNotNull(cobranca);
	}
	
	@Test
	public void cobranca_falhou_inicializar()
	{
		cobranca = null;
		assertNull(cobranca);
		cobranca = cobranca.start();
	}
	

	@Test
	public void deve_enviar_cobranca()
	{
		cobranca = cobranca.start();
		
		Double valor = 1.1;
		String ciclista = "606ae982-09fe-11ec-9a03-0242ac130003";
		Object c = cobranca.cobrar(valor, ciclista);
		Cobranca x = (Cobranca) c;
		assertNotNull(x);
	}
	
	@Test
	public void nao_deve_enviar_cobranca_com_ciclista_igual_a_zero()
	{
		cobranca = cobranca.start();
		
		Double valor = 1.1;
		String ciclista = "0";
		Object c = cobranca.cobrar(valor, ciclista);
		Cobranca x = (Cobranca) c;
		assertNull(x);
	}
	
	@Test
	public void nao_deve_enviar_cobranca_com_ciclista_igual_a_nulo()
	{
		cobranca = cobranca.start();
		
		Double valor = 2.2;
		String ciclista = null;
		Object c = cobranca.cobrar(valor, ciclista);
		ErrorResponse e = (ErrorResponse) c;
		assertEquals("1G", e.codigo );
	}
	
	@Test
	public void nao_deve_enviar_cobranca_com_valor_igual_a_nulo()
	{
		cobranca = cobranca.start();
		
		Double valor = null;
		String ciclista = "555";
		Object c = cobranca.cobrar(valor, ciclista);
		ErrorResponse e = (ErrorResponse) c;
		assertNotNull(e);
	}
	
	@Test
	public void nao_deve_enviar_cobranca_caos_ciclista_nao_esteja_no_banco()
	{
		cobranca = cobranca.start();
		
		Double valor = 5.5;
		String ciclista = "555";
		Object c = cobranca.cobrar(valor, ciclista);
		ErrorResponse e = (ErrorResponse) c;
		assertEquals("1C", e.codigo );
	}
	
	@Test
	public void deve_enviar_cobranca_para_fila_De_cobranca()
	{
		cobranca = cobranca.start();
		
		Double valor = 5.5;
		String ciclista = "teste";
		
		Object c = cobranca.filaDeCobranca(valor, ciclista);
		Cobranca x = (Cobranca) c;
		assertNotNull(x);
	}
	
	@Test
	public void nao_deve_enviar_cobranca_para_fila_de_cobranca_com_ciclista_igual_a_nulo()
	{
		cobranca = cobranca.start();
		
		Double valor = 2.2;
		String ciclista = null;
		Object c = cobranca.filaDeCobranca(valor, ciclista);
		ErrorResponse e = (ErrorResponse) c;
		assertEquals("1G", e.codigo );
	}
	
	@Test
	public void nao_deve_enviar_cobranca_para_fila_de_cobranca_com_valor_igual_a_nulo()
	{
		cobranca = cobranca.start();
		
		Double valor = null;
		String ciclista = "sss";
		Object c = cobranca.filaDeCobranca(valor, ciclista);
		ErrorResponse e = (ErrorResponse) c;
		assertEquals("1G", e.codigo );
	}
	
	@Test
	public void deve_validar_cartao_de_credito()
	{
		cobranca = cobranca.start();
		
		Object c = cobranca.validaCartaoDeCredito("um nome", "5396475798678228", "2020-02-02", "555");
		assertEquals("OK", c);
	}
	
	@Test
	public void nao_deve_validar_cartao_de_credito_com_numero_errado()
	{
		cobranca = cobranca.start();
		
		Object c = cobranca.validaCartaoDeCredito("um nome", "", "2020-02-01", "515");
		ErrorResponse e = (ErrorResponse) c;
		assertEquals("1CC", e.codigo );
	}
	
	@Test
	public void nao_deve_validar_cartao_de_credito_com_data_errada()
	{
		cobranca = cobranca.start();
		
		Object c = cobranca.validaCartaoDeCredito("um nome", "5396475798678228", "2020/02/01", "515");
		ErrorResponse e = (ErrorResponse) c;
		assertEquals("2CC", e.codigo );
	}
	
	@Test
	public void nao_deve_validar_cartao_de_credito_com_cvv_errado()
	{
		cobranca = cobranca.start();
		
		Object c = cobranca.validaCartaoDeCredito("um nome", "5396475798678228", "2020-02-01", "5152233");
		ErrorResponse e = (ErrorResponse) c;
		assertEquals("3CC", e.codigo);
	}
	
	@Test
	public void nao_deve_validar_cartao_de_credito_com_nome_nulo()
	{
		cobranca = cobranca.start();
		
		Object c = cobranca.validaCartaoDeCredito(null, "5396475798678228", "2020-02-01", "5152233");
		ErrorResponse e = (ErrorResponse) c;
		assertEquals("1G", e.codigo );
	}
	
	@Test
	public void nao_deve_validar_cartao_de_credito_com_numero_nulo()
	{
		cobranca = cobranca.start();
		
		Object c = cobranca.validaCartaoDeCredito("um nome", null, "2020-02-01", "5152233");
		ErrorResponse e = (ErrorResponse) c;
		assertEquals("1G", e.codigo );
	}
	
	@Test
	public void nao_deve_validar_cartao_de_credito_com_data_nulo()
	{
		cobranca = cobranca.start();
		
		Object c = cobranca.validaCartaoDeCredito("um nome", "5396475798678228", null, "5152233");
		ErrorResponse e = (ErrorResponse) c;
		assertEquals("1G", e.codigo );
	}
	
	@Test
	public void nao_deve_validar_cartao_de_credito_com_cvv_nulo()
	{
		cobranca = cobranca.start();
		
		Object c = cobranca.validaCartaoDeCredito("um nome", "5396475798678228", "2020-02-01", null);
		ErrorResponse e = (ErrorResponse) c;
		assertEquals("1G", e.codigo );
	}
	
	@Test
	public void deve_retorna_cobranca_pelo_id()
	{
		cobranca = cobranca.start();
		
		Object c = cobranca.pegarCobrancaPeloId("teste");
		assertNotNull(c);
	}
	
	@Test
	public void nao_deve_retorna_cobranca_pelo_id_caso_id_seja_nulo()
	{
		cobranca = cobranca.start();
		
		Object c = cobranca.pegarCobrancaPeloId(null);
		ErrorResponse e = (ErrorResponse) c;
		assertEquals("1G", e.codigo );
	}
	
	@Test
	public void nao_deve_retorna_cobranca_pelo_id_caso_id_nao_esteja_no_banco()
	{
		cobranca = cobranca.start();
		
		Object c = cobranca.pegarCobrancaPeloId("/o\\o/");
		ErrorResponse e = (ErrorResponse) c;
		assertEquals("3C", e.codigo);
	}
}
