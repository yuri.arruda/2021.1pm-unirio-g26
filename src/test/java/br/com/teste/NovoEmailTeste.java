package br.com.teste;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.com.ErrorResponse;
import br.com.modelos.Email;
import br.com.modelos.NovoEmail;
import br.com.servicos.EmailService;

public class NovoEmailTeste 
{	
	@Test
	public void email_Request_init()
	{
		NovoEmail er = new NovoEmail();
		assertNotNull(er);
	}
	
	@Test
	public void email_Request_constructor()
	{
		NovoEmail er = new NovoEmail("email@email", "uma mensagem");
		assertNotNull(er);
	}
	
	
}
