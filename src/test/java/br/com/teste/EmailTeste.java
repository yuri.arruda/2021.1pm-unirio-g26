package br.com.teste;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.com.ErrorResponse;
import br.com.modelos.Email;
import br.com.servicos.EmailService;

public class EmailTeste 
{	
	@Test
	public void email_construtor_mensagem()
	{
		String msg = "Teste mensagem";
		
		Email e = new Email("teste@teste.com", msg);
		assertEquals(msg, e.getMensagem());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void email_construtor_mensagem_nula()
	{
		String msg = null;
		
		Email e = new Email("teste@teste.com", msg);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void email_construtor_email_nula()
	{
		String email = null;
		
		Email e = new Email(email, "Teste mensagem");
		
	}
	
	@Test
	public void email_pega_id()
	{
		Email e = new Email("teste@tese.com", "mensagem");
		
		assertNotNull(e.getId());
	}
	
	@Test
	public void email_pega_email()
	{
		Email e = new Email("teste@tese.com", "mensagem");
		
		assertNotNull(e.getEmail());
	}
	
	@Test
	public void email_pega_mensagem()
	{
		Email e = new Email("teste@tese.com", "mensagem");
		
		assertNotNull(e.getMensagem());
	}
	
	@Test
	public void email_seta_mensagem()
	{
		String mail = "teste@novoteste.com";
		Email e = new Email("teste@tese.com", "mensagem");
		e.setEmail(mail);
		assertEquals(mail, e.getEmail());
	}
	
	@Test
	public void email_seta_email()
	{
		String mensagem = "NOVA MENSAGEM";
		Email e = new Email("teste@tese.com", "mensagem");
		e.setMensagem(mensagem);
		
		assertNotNull(mensagem, e.getMensagem());
	}
}
