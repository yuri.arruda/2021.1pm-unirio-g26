package br.com.teste;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.UUID;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.com.ErrorResponse;
import br.com.modelos.Cobranca;
import br.com.modelos.Email;
import br.com.servicos.EmailService;

public class CobrancaTeste 
{	
	@Test
	public void cobranca_construtor()
	{	
		Cobranca c = new Cobranca("PENDENTE", 0.0, UUID.randomUUID().toString());
		assertEquals("PENDENTE", c.getStatus());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void cobranca_construtor_status_nula()
	{
		Cobranca c = new Cobranca(null, 0.0, UUID.randomUUID().toString());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void cobranca_construtor_valor_negativo()
	{
		Cobranca c = new Cobranca("PENDENTE", -1.1, UUID.randomUUID().toString());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void cobranca_construtor_ciclista_nulo()
	{
		Cobranca c = new Cobranca("PENDENTE", 0.0, null);
	}
	
	@Test
	public void cobranca_getters()
	{	
		Cobranca c = new Cobranca("PENDENTE", 0.0, UUID.randomUUID().toString());
		assertEquals("PENDENTE", c.getStatus());
		assertNotNull(c.getValor());
		assertNotNull(c.getId());
		assertNotNull(c.getCiclista());
		assertNull(c.getHoraFinalizada());
		assertNotNull(c.getHoraSolicitacao());
	}
	
	@Test
	public void cobranca_setters()
	{	
		Cobranca c = new Cobranca("PENDENTE", 0.0, UUID.randomUUID().toString());
		c.setStatus("NOVO");
		c.setValor(2.2);
		c.setHoraFinalizada("hora 1");
		c.setHoraSolicitacao("hora 2");
		c.setCiclista("id");
		
		assertEquals("NOVO", c.getStatus());
		assertEquals("2.2",c.getValor().toString());
		assertEquals("hora 1",c.getHoraFinalizada());
		assertEquals("hora 2", c.getHoraSolicitacao());
		assertEquals("id",c.getCiclista());
	}
}
