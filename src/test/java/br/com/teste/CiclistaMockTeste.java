package br.com.teste;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDateTime;
import java.util.UUID;

import org.junit.Test;

import br.com.modelos.CiclistaMock;


public class CiclistaMockTeste 
{	
	static LocalDateTime date = LocalDateTime.now();

	@Test
	public void ciclista_mock_construtor_mensagem()
	{
		String hora1 = date.toString();
		
		CiclistaMock cm = new CiclistaMock(UUID.randomUUID().toString(), hora1, hora1);
		assertEquals(hora1, cm.getHoraAluguelComeco());
	}
	
	@Test
	public void ciclista_mock_pega_id()
	{
		String hora1 = date.toString();
		CiclistaMock cm = new CiclistaMock(UUID.randomUUID().toString(), hora1, hora1);
		
		assertNotNull(cm.getId());
	}
	
	@Test
	public void ciclista_mock_pega_hora_inicio()
	{
		String hora1 = date.toString();
		CiclistaMock cm = new CiclistaMock(UUID.randomUUID().toString(), hora1, hora1);
		
		assertNotNull(cm.getHoraAluguelComeco());
	}
	
	@Test
	public void ciclista_mock_pega_hora_final()
	{
		String hora1 = date.toString();
		CiclistaMock cm = new CiclistaMock(UUID.randomUUID().toString(), hora1, hora1);
		
		assertNotNull(cm.getHoraAluguelFim());
	}
	
	
	@Test
	public void ciclista_mock_seta_hora_final()
	{
		String hora1 = date.toString();
		CiclistaMock cm = new CiclistaMock(UUID.randomUUID().toString(), hora1, hora1);
		
		String hora2 = date.toString();
		cm.setGetHoraAluguelFim(hora2);
		
		assertNotNull(hora2, cm.getHoraAluguelFim());
	}
}
