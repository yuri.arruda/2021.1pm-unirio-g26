package br.com;

import static io.javalin.apibuilder.ApiBuilder.get;
import static io.javalin.apibuilder.ApiBuilder.post;

import io.javalin.Javalin;
import io.javalin.plugin.openapi.OpenApiOptions;
import io.javalin.plugin.openapi.OpenApiPlugin;
import io.javalin.plugin.openapi.ui.ReDocOptions;
import io.javalin.plugin.openapi.ui.SwaggerOptions;
import io.swagger.v3.oas.models.info.Info;

import br.com.controller.*;


public class Builder 
{
	private Javalin builderApp = Javalin.create(config -> {
		config.registerPlugin(getConfiguredOpenApiPlugin());
		config.defaultContentType = "application/json";
	}).routes(() -> 
	{ 
		post("/enviaEmail", EmailController::create); 
		post("/cobranca", CobrancaController::cobrar);
		post("/filaCobranca", CobrancaController::filaCobranca);
		get("/cobranca/:idCobranca", CobrancaController::pegarCobranca);
		post("/validaCartaoDeCredito", CobrancaController::validaCC);
	});

	public void start(int port)
	{
		if (port == 0)
			builderApp.start(getHerokuAssignedPort());
		else
			builderApp.start(port);
	}
	
	private static int getHerokuAssignedPort()
	{
		ProcessBuilder processBuilder = new ProcessBuilder();
		if (processBuilder.environment().get("PORT") != null)
		{
			return Integer.parseInt(processBuilder.environment().get("PORT"));
		}
		return 7000;
	}
	
	private static OpenApiPlugin getConfiguredOpenApiPlugin() 
	{
		Info info = new Info().version("1.0").title("Bicicletário :: API")
			.description("Sistema de controle de bicicletário");
		OpenApiOptions options = new OpenApiOptions(info).activateAnnotationScanningFor("br.com")
			.path("/swagger-docs") // endpoint for OpenAPI json
			.swagger(new SwaggerOptions("/")) // endpoint for swagger-ui
			.reDoc(new ReDocOptions("/redoc")); // endpoint for redoc
		return new OpenApiPlugin(options);
	}
}
