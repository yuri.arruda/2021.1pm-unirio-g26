package br.com.servicos;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import br.com.ErrorResponse;
import br.com.modelos.Email;

public class EmailService 
{
	private static ArrayList<Email> emails = new ArrayList<>();
	private static ArrayList<ErrorResponse> error = new ArrayList<>();
	private static final Logger LOGGER = Logger.getLogger(EmailService.class.getName());

	static 
	{
		emails.add(new Email("yuri.arruda@uniriotec.br", "Teste de mensagem01"));
		emails.add(new Email("email02@uniriotec.br", "Teste de mensagem02"));
		emails.add(new Email("email03@uniriotec.br", "Teste de mensagem03"));
		emails.add(new Email("email04@uniriotec.br", "Teste de mensagem04"));
		emails.add(new Email("email04@uniriotec.br", "Teste de mensagem04"));
		
		error.add(new ErrorResponse("1E", "Erro, e-mail não existe no banco de dados"));
		error.add(new ErrorResponse("2E", "Erro, e-mail invalido"));
		error.add(new ErrorResponse("3E", "Ops, algo deu errado"));
		error.add(new ErrorResponse("1G", "Dados inválidos"));
	}

	private static EmailService emailB = null;
	
	private EmailService() {};
	
	public static void stop()
	{
		emailB = null;
	}
	
	public static EmailService start()
	{
		if (emailB == null)
		{
			emailB = new EmailService();
			return emailB;
		}
		else
		{
			return emailB;
		}
	}
	
	public ArrayList<Email> getAllEmails()
	{
		return emails;
	}
	
	
	public boolean isEmailExist(String e)
	{
		LOGGER.log(Level.INFO,"EmailService - isEmailExist - Verificando e-mail: [{0}] na base", e);
		if (e == null || e.isEmpty())
		{
			return false;
		}
		List<Email> email = emails.stream().filter(foo -> foo.getEmail().equals(e)).collect(Collectors.toList());
		LOGGER.log(Level.INFO,"EmailService - isEmailExist - Quantidade de email é: {0}", emails.size());
		if(!email.isEmpty() || email.size() == 1)
		{
			return true;
		}
		else return false;
		
	}
	
	public boolean isEmailValid(String e)
	{
		LOGGER.log(Level.INFO,"EmailService - isEmailValid - Verificando a validade do e-mail: {0}", e);
		if (e == null || e.isEmpty())
		{
			return false;
		}
		else
		{
			try
			{
				InternetAddress ea = new InternetAddress (e);
				ea.validate();
			}
			catch (AddressException ex) 
			{
				return false;
			}
			return true;
		}
	}
	
	public Object sendEmail(String email, String mensagem)
	{
		LOGGER.log(Level.INFO,"EmailService - sendEmail - Enviando email");
		if (email == null || mensagem == null)
		{
			return error.get(3);
		}
		
		if (!isEmailValid(email))
		{
			return error.get(1);
		}
		if (!isEmailExist(email))
		{
			return error.get(0);
		}
		try 
		{
			Mailer.sendAsHtml(email, mensagem);
			Email e = emails.stream()
			        .filter(foo -> foo.getEmail().equals(email))
			        .collect(toSingleton());
			e.setMensagem(mensagem);
			return e;
		} 
		catch (MessagingException e) 
		{
			e.printStackTrace();
		}
		return error.get(3);
	}

	private static <T> Collector<T, ?, T> toSingleton() {
	    return Collectors.collectingAndThen(
	            Collectors.toList(),
	            list -> {
	                if (list.size() != 1) {
	                    throw new IllegalStateException();
	                }
	                return list.get(0);
	            }
	    );
	}

}
