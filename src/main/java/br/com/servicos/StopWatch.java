package br.com.servicos;

import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;


public class StopWatch 
{
	private static final Logger LOGGER = Logger.getLogger(StopWatch.class.getName());
	private int segundos = 0;
	public boolean cobranca = false;
	CobrancaService cs = new CobrancaService();
	int s = 0, m = 0, h = 0;
	
	public Timer timer = null;
    public TimerTask task = null;
	
    public StopWatch() 
    {
    	timer = new Timer();
    }
    public void start()
    {
    	task = new TimerTask()
    	{
    		@Override
    		public void run()
    		{
    			segundos++;
    			if (segundos == 43200)
    	  		{
    				cs.filaDeCobranca(0.0, "0");
    				segundos = 0;
    	  		}
    			LOGGER.log(Level.INFO, "[INFO] - StopWatch - run - Verificando tempo para puxar cobranças da lista: segundos faltando {0}", 43200 - segundos );
    		}
  		};
  		timer.scheduleAtFixedRate(task, 0, 1000);
    }
    
    public void stop()
    {
    	 task.cancel();
         timer.cancel();
         timer.purge();
    }
    
    public void restart()
    {
    	stop();
        start();
    }
}
