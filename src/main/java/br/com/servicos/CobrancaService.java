package br.com.servicos;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Level;
import java.util.logging.Logger;

import br.com.ErrorResponse;
import br.com.modelos.CiclistaMock;
import br.com.modelos.Cobranca;
import br.com.moip.validators.CreditCard;

public class CobrancaService 
{
	private static ArrayList<Cobranca> cobranca = new ArrayList<>();
	private static ArrayList<ErrorResponse> error = new ArrayList<>();
	private static ArrayList<CiclistaMock> ciclistaMock = new ArrayList<>();
	private ZoneId zone = ZoneId.systemDefault();
	static LocalDateTime date = LocalDateTime.now();
	private String hdc =  "0"; 
	private static final Logger LOGGER = Logger.getLogger(CobrancaService.class.getName());
	
	static 
	{
		cobranca.add(new Cobranca("0", 0.0, "0"));
		cobranca.add(new Cobranca("PAGA", 0.0, UUID.randomUUID().toString()));
		cobranca.add(new Cobranca("CANCELADA", 0.0, UUID.randomUUID().toString()));
		cobranca.add(new Cobranca("PENDENTE", 0.0, "606ae982-09fe-11ec-9a03-0242ac130003"));
		cobranca.add(new Cobranca("FALHA", 0.0, "teste"));
		cobranca.add(new Cobranca());
		
		error.add(new ErrorResponse("1C", "Erro ao efetuar a cobrança"));
		error.add(new ErrorResponse("2C", "Erro ao Incluir cobrança na fila"));
		error.add(new ErrorResponse("3C", "Erro ao pegar cobrança"));
		error.add(new ErrorResponse("1CC", "Número do catão de crédito é invalido"));
		error.add(new ErrorResponse("2CC", "Data de validade do catão de crédito é inválido"));
		error.add(new ErrorResponse("3CC", "Código de segurança do cartão de crédito é inválido"));
		error.add(new ErrorResponse("1G", "Dados inválidos"));
		
		ciclistaMock.add(new CiclistaMock("606ae982-09fe-11ec-9a03-0242ac130003", date.toString(), ""));
	}

	private static CobrancaService cobrancaB = null;
	
	CobrancaService() {};
	
	public static void stop()
	{
		cobrancaB = null;
	}
	
	public static CobrancaService start()
	{
		if (cobrancaB == null)
		{
			cobrancaB = new CobrancaService();
			return cobrancaB;
		}
		else
		{
			return cobrancaB;
		}
	}
	
	private boolean isCiclistaExist(String c)
	{
		LOGGER.log(Level.INFO," CobrancaService - isCiclistaExist - Verificando ciclista [{0}] na base de cobrança", c);
		if (c == null || c.isEmpty())
		{
			return false;
		}
		List<Cobranca> cobrancas = cobranca.stream()
				.filter(foo -> foo.getCiclista().equals(c))
				.collect(Collectors.toList());
		LOGGER.log(Level.INFO, "CobrancaService - isCiclistaExist - Lista de ciclista em cobranca contem : {0}", cobrancas.size());
		if(!cobrancas.isEmpty() || cobrancas.size() == 1)
		{
			return true;
		}
		else return false;
		
	}
	
	private String getCiclistaHoraAluguelComeco(String id)
	{
		return ciclistaMock.stream()
		        .filter(foo -> foo.getId().equals(id))
		        .collect(toSingleton()).getHoraAluguelComeco();
		
	}
	
	private String getCiclistaHoraAluguelFinal(String id)
	{
		int randomNum = ThreadLocalRandom.current().nextInt(0, 20 + 1);
		LocalDateTime time = LocalDateTime.now().plusHours(randomNum);
		
		CiclistaMock cm = ciclistaMock.stream()
		        .filter(foo -> foo.getId().equals(id))
		        .collect(toSingleton());
		
		cm.setGetHoraAluguelFim(time.toString());
		
		return cm.getHoraAluguelFim();
	}
	
	private Double extraTaxes(Cobranca c, double valor)
	{
		LOGGER.log(Level.INFO, "CobrancaService - extraTaxes - Verificando taxas extas");
		
		String comeco = getCiclistaHoraAluguelComeco(c.getCiclista());
		String fim = getCiclistaHoraAluguelFinal(c.getCiclista());
		
		long epochS = LocalDateTime.parse(comeco).atZone(zone).toEpochSecond() / 60;
		long epochF = LocalDateTime.parse(fim).atZone(zone).toEpochSecond() / 60;
		LOGGER.log(Level.INFO, "CobrancaService - extraTaxes - Hora inicial (seg) {0}", epochS);
		LOGGER.log(Level.INFO, "CobrancaService - extraTaxes - Hora Final (seg) {0}", epochF);
		long makingMoney = epochF - epochS;
		LOGGER.log(Level.INFO, "CobrancaService - extraTaxes - Minutos desde a cobrança (min) {0}", makingMoney);
		if (makingMoney <= 120) 
		{
			return 0.0;
		}
		else 
		{
			int extra = (int) ((makingMoney - 120) / 30);
			return (5.0 * extra) + valor;
		}
	} 
	
	public Object cobrar(Double valor, String ciclista) 
	{
		LocalDateTime dataDaCobrancaFinal = LocalDateTime.now();
		if (valor == null || ciclista == null)
		{
			return error.get(6);
		}
		if (ciclista.equals("0")) return null;
		LOGGER.log(Level.INFO, "CobrancaService - cobrar - efetuando cobrança para [{0}] no valor de ", valor);
		
		
		if (!isCiclistaExist(ciclista)) 
		{
			return error.get(0);
		}
		
		Cobranca c = cobranca.stream()
		        .filter(foo -> foo.getCiclista().equals(ciclista))
		        .collect(toSingleton());
		
		LOGGER.log(Level.INFO, "CobrancaService - cobrar - Status da cobranca é: {0}", c.getStatus());
		//supondo que o ato de cobrar finalize a cobrança e gere a data finalizada
		c.setHoraFinalizada(dataDaCobrancaFinal.toString());
		if(!c.getStatus().equals("PAGO"))
		{
			c.setValor(extraTaxes(c, valor));
		}
		return c;
	}
	
	public Object filaDeCobranca(Double valor, String ciclista) 
	{
		LOGGER.log(Level.INFO, "CobrancaService - filaDeCobranca - Adicionando a fila de cobrança para [{0}] no valor de ", valor);
		if (valor == null || ciclista == null)
		{
			return error.get(6);
		}
		if (ciclista.equals("0"))
		{
			Cobranca c = cobranca.stream()
			        .filter(foo -> foo.getCiclista().equals(ciclista))
			        .collect(toSingleton());
			
			LOGGER.log(Level.INFO, "CobrancaService - filaDeCobranca - Rodando a bateria de cobranças");
			for (int index = 0; index <= cobranca.size() - 1; index++)
			{
				if (c.getCiclista().equals(hdc))
				{
					cobrar(cobranca.get(index).getValor(), cobranca.get(index).getCiclista());
				}
			}
			return null;
		}
		
		LocalDateTime dataDaCobrancaFinal = LocalDateTime.now();
		
		if (!isCiclistaExist(ciclista)) 
		{
			return error.get(1);
		}
		
		cobranca.add(new Cobranca("PENDENTE", valor, ciclista));
		Cobranca c = cobranca.get(cobranca.size() - 1);
		c.setHoraFinalizada(dataDaCobrancaFinal.toString());
		
		return cobranca.get(cobranca.size() - 1);
	}
	
	public Object pegarCobrancaPeloId(String id)
	{
		if (id == null)
		{
			return error.get(6);
		}
		LOGGER.log(Level.INFO, "CobrancaService - pegarCobrancaPeloId - Buscando cobrança pelo id [{0}]", id);
		List<Cobranca> c = cobranca.stream()
				.filter(foo -> foo.getId().compareTo(id) == 0)
				.collect(Collectors.toList());
		if (c.size() == 1)
		{
			return c;
		}
		else
		{
			return error.get(2);
		}
	}
	
	public Object validaCartaoDeCredito(String nomeTitular, String numero, String validade, String cvv)
	{
		LOGGER.log(Level.INFO, "CobrancaService - validaCartaoDeCredito - validando cartão de credito");
		if (nomeTitular == null || numero == null || validade == null || cvv == null)
		{
			return error.get(6);
		}
		
		boolean cc = new CreditCard(numero).isValid();
		boolean dataValidade = validade.matches("^\\d{4}\\-(0[1-9]|1[012])\\-(0[1-9]|[12][0-9]|3[01])");
		boolean cs = cvv.matches("\\b\\d{3}\\b");
		if (!cc)
		{
			return error.get(3);
		}
		if (!dataValidade)
		{
			return error.get(4);
		}
		if (!cs)
		{
			return error.get(5);
		}
		return "OK";
	}
	
	private static <T> Collector<T, ?, T> toSingleton() {
	    return Collectors.collectingAndThen(
	            Collectors.toList(),
	            list -> {
	                if (list.size() != 1) {
	                    throw new IllegalStateException();
	                }
	                return list.get(0);
	            }
	    );
	}
}
