package br.com.servicos;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

public final class Mailer 
{
	private Mailer() {}
	private static final String SENDEREMAIL = "bicicletariounirio@gmail.com";
	private static final String SENDERPASSWORD = "unirio.pm.2021";
	
	public static void sendAsHtml(String to, String html) throws MessagingException
	{	
	    Session session = createSession();
	
	    //create message using session
	    MimeMessage message = new MimeMessage(session);
        prepareEmailMessage(message, to, "Mensagem automática do bicicletario da UNIRIO", html);	
	    //sending message
	    Transport.send(message);
	}
	
	private static void prepareEmailMessage(MimeMessage message, String to, String title, String html)
	        throws MessagingException 
	{
	    message.setContent(html, "text/html; charset=utf-8");
	    message.setFrom(new InternetAddress(SENDEREMAIL));
	    message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
	    message.setSubject(title);
	}
	
	private static Session createSession() 
	{
	    Properties props = new Properties();
	    props.put("mail.smtp.host", "smtp.gmail.com");
	    props.put("mail.smtp.socketFactory.port", "465");
	    props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
	    props.put("mail.smtp.auth", "true");
	    props.put("mail.smtp.port", "465");
	    props.put("mail.smtp.ssl.checkserveridentity", true);
	
	    Session session = Session.getInstance(props, new javax.mail.Authenticator()
	    {
	    	@Override
	        protected PasswordAuthentication getPasswordAuthentication() 
	        {
	            return new PasswordAuthentication(SENDEREMAIL, SENDERPASSWORD);
	        }
	    });
	    return session;
	}
	
}
