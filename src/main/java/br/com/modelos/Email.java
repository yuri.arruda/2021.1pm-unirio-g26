package br.com.modelos;

import java.util.UUID;

public class Email 
{
	
	private String id;
	private String userEmail;
	private String mensagem;

	public Email(String userEmail, String mensagem)
	{
		if(userEmail == null) throw new IllegalArgumentException("E-mail não pode ser nula");
		if(mensagem == null) throw new IllegalArgumentException("Mensagem não pode ser nula");
			
		this.id = UUID.randomUUID().toString();
		this.userEmail = userEmail;
		this.mensagem = mensagem;
	}
	
	
	public String getId() { return this.id; }

	public String getEmail() {return this.userEmail; }
	
	public String getMensagem() {return this.mensagem; }
	
	public void setEmail(String email) { this.userEmail = email; }
	
	public void setMensagem(String mensagem) {this.mensagem = mensagem; }
}
