package br.com.modelos;

import java.time.LocalDateTime;
import java.util.UUID;

public class Cobranca 
{
	private String id;
	private String status;
	private String horaSolicitacao;
	private String horaFinalizada;
	private Double valor;
	private String ciclista;
	
	public Cobranca(String status, Double valor, String ciclista)
	{
		LocalDateTime dataDaCobranca = LocalDateTime.now();
		if(status == null) throw new IllegalArgumentException("Status não pode ser nula");
		if(valor < 0) throw new IllegalArgumentException("Valor não pode ser negativo");
		if(ciclista == null) throw new IllegalArgumentException("Id do ciclista não pode ser nula");
			
		this.id = UUID.randomUUID().toString();
		this.status = status;
		this.horaSolicitacao = dataDaCobranca.toString();
		this.horaFinalizada = null;
		this.valor = valor;
		this.ciclista = ciclista;
	}
	
	public Cobranca()
	{
		this.id = "teste";
		this.status = "";
		this.horaSolicitacao = "";
		this.horaFinalizada = "";
		this.valor = 0.0;
		this.ciclista = "";
	}

	public String getId() 
	{
		return id;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public String getHoraSolicitacao() 
	{
		return horaSolicitacao;
	}

	public void setHoraSolicitacao(String horaSolicitacao) 
	{
		this.horaSolicitacao = horaSolicitacao;
	}

	public String getHoraFinalizada()
	{
		return horaFinalizada;
	}

	public void setHoraFinalizada(String horaFinalizada) 
	{
		this.horaFinalizada = horaFinalizada;
	}

	public Double getValor() 
	{
		return valor;
	}

	public void setValor(Double valor) 
	{
		this.valor = valor;
	}

	public String getCiclista()
	{
		return ciclista;
	}

	public void setCiclista(String ciclista)
	{
		this.ciclista = ciclista;
	}
}

