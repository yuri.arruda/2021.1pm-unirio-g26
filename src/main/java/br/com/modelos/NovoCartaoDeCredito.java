package br.com.modelos;

public class NovoCartaoDeCredito 
{
	public String nomeTitular;
	public String numero;
	public String validade;
	public String cvv;
	
	public NovoCartaoDeCredito()
	{
		
	}
	
	public NovoCartaoDeCredito(String nomeTitular, String numero, String validade, String cvv)
	{	
		this.nomeTitular = nomeTitular;
		this.numero = numero;
		this.validade = validade;
		this.cvv = cvv;
	}
}

