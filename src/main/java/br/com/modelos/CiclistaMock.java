package br.com.modelos;

public class CiclistaMock 
{
	
	private String id;
	private String horaAluguelComeco;
	private String horaAluguelFim;

	public CiclistaMock(String id, String horaAluguelComeco, String horaAluguelFim)
	{		
		this.id = id;
		this.horaAluguelComeco = horaAluguelComeco;
		this.horaAluguelFim = horaAluguelFim;
	}

	public String getId() 
	{
		return id;
	}

	public String getHoraAluguelComeco() 
	{
		return horaAluguelComeco;
	}


	public String getHoraAluguelFim() 
	{
		return horaAluguelFim;
	}
	
	public void setGetHoraAluguelFim(String horaAluguelFim)
	{
		this.horaAluguelFim = horaAluguelFim;
	}
}

