package br.com;

import java.util.UUID;

public class ErrorResponse 
{
	public String id;
	public String codigo;
	public String mensagem;
	
	public ErrorResponse (String codigo, String mensagem)
	{
		id = UUID.randomUUID().toString();
		this.codigo = codigo;
		this.mensagem = mensagem;
	}
}
