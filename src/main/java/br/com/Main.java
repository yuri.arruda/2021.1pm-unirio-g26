package br.com;

import br.com.servicos.StopWatch;

public class Main 
{
	public static void main(String[] args) 
    {
		StopWatch sw = new StopWatch();
		sw.start();
        
		Builder build = new Builder();
		build.start(0);
	}
}
