package br.com.controller;

import io.javalin.http.Context;
import io.javalin.plugin.openapi.annotations.HttpMethod;
import io.javalin.plugin.openapi.annotations.OpenApi;
import io.javalin.plugin.openapi.annotations.OpenApiContent;
import io.javalin.plugin.openapi.annotations.OpenApiRequestBody;
import io.javalin.plugin.openapi.annotations.OpenApiResponse;

import br.com.servicos.EmailService;
import br.com.ErrorResponse;
import br.com.modelos.*;


public final class EmailController
{
	private EmailController()
	{}

	@OpenApi(
		path = "/enviaEmail",
		method = HttpMethod.POST, 
		summary = "Notificar via email", 
		tags = {"externo"}, 
		requestBody =  @OpenApiRequestBody(content = {@OpenApiContent(from = NovoEmail.class)}), 
		responses 	= {@OpenApiResponse(status = "200", description = "Externo solicitada"),
					   @OpenApiResponse(status = "404" ,content = {@OpenApiContent(from = ErrorResponse.class)}),
					   @OpenApiResponse(status = "422" ,content = {@OpenApiContent(from = ErrorResponse.class)})})
	public static void create(Context ctx)
	{	
		EmailService email = EmailService.start();
		try
		{
			NovoEmail req = ctx.bodyAsClass(NovoEmail.class);
			Object e = email.sendEmail(req.email, req.mensagem);
			
			if (e == null)
			{
				ctx.html("Error").status(500);
			}
			else if(e instanceof ErrorResponse)
			{
				ctx.json(e).status(404);
			}
			else
			{
				ctx.json(e).status(200);
			}
		}
		catch (NullPointerException err) 
		{
			ctx.json(err);
		}
		EmailService.stop();
	}
}
