package br.com.controller;

import br.com.servicos.CobrancaService;
import io.javalin.http.Context;
import io.javalin.plugin.openapi.annotations.HttpMethod;
import io.javalin.plugin.openapi.annotations.OpenApi;
import io.javalin.plugin.openapi.annotations.OpenApiContent;
import io.javalin.plugin.openapi.annotations.OpenApiRequestBody;
import io.javalin.plugin.openapi.annotations.OpenApiResponse;

import br.com.ErrorResponse;
import br.com.modelos.*;

public final class CobrancaController 
{
	private static String error = "Error!";
	private CobrancaController()
	{}
	
	@OpenApi(
		path = "/cobranca",
		method = HttpMethod.POST, 
		summary = "Realizar cobrança", 
		tags = {"externo"}, 
		requestBody =  @OpenApiRequestBody(content = {@OpenApiContent(from = NovaCobranca.class)}), 
		responses 	= {@OpenApiResponse(status = "200", description = "Cobrança solicitada"),
					   @OpenApiResponse(status = "422", description = "Dados Inválidos", content = {@OpenApiContent(from = ErrorResponse.class)})})
	public static void cobrar(Context ctx)
	{	
		CobrancaService cobrar = CobrancaService.start();
		try
		{
			NovaCobranca cobranca = ctx.bodyAsClass(NovaCobranca.class);
			Object c = cobrar.cobrar(cobranca.valor, cobranca.ciclista);
			if(c == null)
			{
				ctx.html(error).status(500);
			}
			else if (c instanceof ErrorResponse)
			{
				ctx.json(c).status(422);
			}
			else
			{
				ctx.json(c).status(200);
			}
		}
		catch (Exception err) 
		{
			ctx.json(err);
		}
		CobrancaService.stop();
	}
	
	@OpenApi(
		path = "/filaCobranca",
		method = HttpMethod.POST, 
		summary = "Inclui cobrança na fila de cobrança. Cobranças na fila serão cobradas de tempos em tempos", 
		tags = {"externo"}, 
		requestBody =  @OpenApiRequestBody(content = {@OpenApiContent(from = NovaCobranca.class)}), 
		responses 	= {@OpenApiResponse(status = "200", description = "Cobrança Incluida"),
					   @OpenApiResponse(status = "422", description = "Dados Inválidos", content = {@OpenApiContent(from = ErrorResponse.class)})})
	public static void filaCobranca(Context ctx)
	{
		CobrancaService cobrar = CobrancaService.start();
		try
		{
			NovaCobranca cobranca = ctx.bodyAsClass(NovaCobranca.class);
			Object c = cobrar.filaDeCobranca(cobranca.valor, cobranca.ciclista);
			if(c == null)
			{
				ctx.html(error).status(500);
			}
			else if (c instanceof ErrorResponse)
			{
				ctx.json(c).status(422);
			}
			else
			{
				ctx.json(c).status(200);
			}
		}
		catch (Exception err)
		{
			ctx.json(err);
		}
		
		CobrancaService.stop();
	}
	
	@OpenApi(
		path = "/cobranca/:idCobranca",
		method = HttpMethod.GET, 
		summary = "Obter cobrança", 
		tags = {"externo"}, 
		responses 	= {@OpenApiResponse(status = "200", description = "Cobrança solicitada"),
					   @OpenApiResponse(status = "404", description = "Não encontrado", content = {@OpenApiContent(from = ErrorResponse.class)})})
	public static void pegarCobranca(Context ctx)
	{
		CobrancaService cobrar = CobrancaService.start();
		String id = ctx.pathParam("idCobranca");
		try
		{
			Object c = cobrar.pegarCobrancaPeloId(id);
			if(c == null)
			{
				ctx.html(error).status(500);
			}
			else if (c instanceof ErrorResponse)
			{
				ctx.json(c).status(422);
			}
			else
			{
				ctx.json(c).status(200);
			}
		}
		catch (Exception err)
		{
			ctx.json(err);
		}
	}
	
	@OpenApi(
		path = "/validaCartaoDeCredito",
		method = HttpMethod.POST, 
		summary = "Valida um cartão de crédito", 
		tags = {"externo"}, 
		requestBody =  @OpenApiRequestBody(content = {@OpenApiContent(from = NovoCartaoDeCredito.class)}), 
		responses 	= {@OpenApiResponse(status = "200", description = "Dados atualizados"),
					   @OpenApiResponse(status = "422",  content = {@OpenApiContent(from = ErrorResponse.class)})})
	public static void validaCC(Context ctx)
	{
		CobrancaService cobrar = CobrancaService.start();
		try
		{
			NovoCartaoDeCredito cc = ctx.bodyAsClass(NovoCartaoDeCredito.class);
			Object c = cobrar.validaCartaoDeCredito(cc.nomeTitular, cc.numero, cc.validade, cc.cvv);
			if (c.equals("OK"))
			{
				ctx.html("Dados atualizados").status(200);
			}
			else
			{
				ctx.json(c).status(404);
			}
			
		}
		catch (Exception err)
		{
			ctx.json(err);
		}
		
		CobrancaService.stop();
	}
	
}
